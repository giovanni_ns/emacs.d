(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(setq ring-bell-function 'ignore)
(display-line-numbers-mode 1)
(column-number-mode 1)

;;; Atom one dark
(load-file "$HOME/.emacs.d/temas/atom-one-dark-theme/atom-one-dark-theme.el")
(custom-set-variables
 '(custom-safe-themes
   '("0c860c4fe9df8cff6484c54d2ae263f19d935e4ff57019999edbda9c7eda50b8" default)))
(custom-set-faces)
(load-theme 'atom-one-dark)
